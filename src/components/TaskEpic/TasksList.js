import React from 'react'

import { FlatList } from 'react-native'

import TasksTile from './TasksTile'



const TasksList = ({tasks, onChangeStatus, onDeleteTask}) => {
  const _renderItem = ({ item }) => (
  <TasksTile 
    id={item.id}     
    title={item.title}
    completed={item.completed}
    onChangeStatus={onChangeStatus}
    onDeleteTask={onDeleteTask}>
  </TasksTile>)
  
  return (
        <FlatList
            data={tasks}
            renderItem={_renderItem}
            keyExtractor={item => item.title}
        />
    )
}

export default TasksList
